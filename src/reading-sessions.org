#+TITLE: Reading Sessions
#+AUTHOR: Devi Prasad & Thirumal Ravula
#+DATE: [2017-09-17 Sun]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  en
#+OPTIONS:   h:6 ^:nil
#+XSLT:

#+LaTeX_HEADER: \usepackage{titlesec}
#+LaTeX_HEADER: \setcounter{secnumdepth}{5}
#+LaTeX_HEADER: \usepackage{siunitx}
#+LaTeX_HEADER: \usepackage[T1]{fontenc}
#+LaTeX_HEADER: \usepackage{mathpazo}
#+LaTeX_HEADER: \usepackage[section]{placeins}

#+LaTeX_HEADER: \usepackage{booktabs}
#+LaTeX_HEADER: \linespread{1.05}
#+LaTeX_HEADER: \usepackage[scaled]{helvet}
#+LaTeX_HEADER: \usepackage{courier}
#+LaTeX_HEADER: \usepackage{varioref}
#+LaTeX_HEADER: \usepackage[usenames,dvipsnames]{color}
#+LaTeX_HEADER: \usepackage[usenames,svgnames]{xcolor}
#+LaTeX_HEADER: \usepackage{colortbl}
#+LaTeX_HEADER: \usepackage{fancyvrb}
#+LaTeX_HEADER: \usepackage{tfrupee}  
#+LaTeX_HEADER: \usepackage{everypage}
#+LaTeX_HEADER: \usepackage[section]{placeins}
#+LaTeX_HEADER: \usepackage{rotating}


#+LaTeX_HEADER: \usepackage{hyperref}
#+LaTeX_HEADER: \hypersetup{colorlinks=true,linkcolor=blue,urlcolor=RawSienna}
#+LaTeX_HEADER: \newcommand{\hilight}[1]{\colorbox{yellow}{#1}}
#+LaTeX_HEADER: \newcommand{\revised}[1]{\protect{\colorbox{yellow}{#1}}}

#+LaTeX_HEADER: \newcommand{\nbsp}{~}
#+LaTeX_HEADER: \setcounter{section}{0}

#+LaTeX_HEADER: \usepackage{caption}
#+LaTeX_HEADER: \DeclareCaptionLabelFormat{myformat}{\Large{\color{Blue}{#1}~\textbf{#2}}}
#+LaTeX_HEADER: \captionsetup{labelformat=myformat}

#+LaTeX_HEADER: \DeclareCaptionFont{red}{\color{red}}
#+LaTeX_HEADER: \DeclareCaptionFont{green}{\color{green}}
#+LaTeX_HEADER: \DeclareCaptionFont{yellow}{\color{yellow}}
#+LaTeX_HEADER: \DeclareCaptionFont{blue}{\color{blue}}
#+LaTeX_HEADER: \DeclareCaptionFont{foo}{\color{red}}


#+LaTeX_HEADER: \DeclareCaptionFormat{box}{\colorbox[rgb]{0.9,0.9,0.9}{\parbox{\textwidth}{#1#2#3}}}
#+LaTeX_HEADER: \newcommand{\hilightcaption}{\captionsetup{format=box}}


#+LaTeX_HEADER: \newcommand{\normalcaption}{\captionsetup{labelformat=myformat, format=plain}}
#+LaTeX_HEADER: \newcommand{\h}[1]{\protect{\bf {#1}}}


#+LATEX: \listoftables

\hilightcaption
#+LaTeX_HEADER: \usepackage{pgf}
#+BEGIN_LaTeX
\sisetup{
    group-digits=true,
    group-four-digits=true,
    group-separator={,\ },
}
#+END_LaTeX

#+OPTIONS: ':nil *:t -:t ::t <:t H:4 \n:nil ^:t
#+OPTIONS: arch:headline author:t c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t
#+OPTIONS: inline:t num:t p:nil pri:nil prop:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t title:t toc:t
#+OPTIONS: todo:t |:t
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.3.1 (Org mode 8.3.4)

* Introduction
  This document talks about reading clubs - the necessity,
  purpose, what exists, how to build one - and all the
  banality around it.

* Necessity
  Multiple constraints act as barriers to get people to
  read.  Lack of physical libraries puts a dampener on the
  initiation to reading.  Not only do the libraries serve as
  a repository to books, but also allow kids, youth and
  elders to start a conversation.  With single kids and
  nuclear families and the infiltration of smart phones
  there is an urgency for stronger and saner communities.

* What exists  
  Books that are out of copyright are available on the
  internet.  [[https://openlibrary.org/][OpenLibrary]] is one such resource.  Then there
  are also physical books with each individual.  Portals
  like [[https://www.goodreads.com][goodreads]] provide community recommendation services.
  [[https://www.meetup.com][Meetup]] services allow people with similar interests to
  meet and engage in an activity.  [[https://en.wikipedia.org/wiki/BitTorrent][BitTorrent]] facilitates
  sharing an individual's files with others.  The goal is to
  conjure an analogous mash up of all the above services.

* A platform supporting reading sessions

** Session Initiation
  A reading session is initiated by any member by suggesting
  a book, a date and a place.  Other members can agree or
  make suggestions on the book within some time frame and
  the initiator based upon either the votes or on some other
  information finalizes the book.  Other members can confirm
  their participation in the reading session.

** Virtual Book Shelf
   Each member meanwhile has a virtual shelf built.  The
   platform allows building a catalog for each user with the
   ability for the owner of the shelf to choose the
   visibility of each book.  The shelf is a projection of
   individual's physical book shelves.

** Reading Rooms
   The platform allows managing the physical rooms to
   conduct the reading sessions.  Each room has a manager
   who ensures the availability of the room for the session.
   During the initiation of a session, a go ahead from the
   room manager is mandatory.

** A Torrent like library
   The platform provides the ability for the users attending
   a reading session to look at other members' virtual shelf
   and request a book loan.  The physical exchange of the
   books happen during the session.

** Commercial Angle
   These are some of the activities supported by the
   platform to help generate revenue.
*** Book Tours
    The platform provides the ability for the publishing
    houses to set up reading sessions by the authors of new
    books.  A cost model can be published.
*** Audio Books
    Produce and sell audio books in local languages.  Some
    users can understand and talk in a language but not
    read.  Audio books could bridge the shortcoming.

*** Geriatric Reading or Broadcasted Reading
    People who are good 

** Other paraphernalia
*** Map service   
    Routing service to the location where the reading
    session is held.
*** Discussion forums
    Each reading session upon initiation gets a discussion
    board.
*** Review boards
    Users have the ability to review the books on the
    virtual shelves.  The user must have the option to
    choose reviews from other external open sources, say
    like [[https://goodreads.com][goodreads]]

*** Infrasturcture for writing books
