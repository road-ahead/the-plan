#+TITLE: Switch to a Contract
#+AUTHOR: Thirumal Ravula
#+DATE: [2018-11-22 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


Dear Venkatesh,

I take this opportunity to let you know of my plans with
respect to my contribution at VLEAD.  I have been working
full time all this while at VLEAD.  When I was first
interviewed for the VLEAD position, I guaranteed 2 years of
my time at VLEAD before I moved on with my other plans.
Those 2 years extended to another 2 more years while my
other plans endeared a more theoretical underbearing that
now need realization.  At the same time, we had another kid
that needs some of my time.

Under these circumstances, I came to realize that it is fair
to restrict my contribution at VLEAD to a certain fixed time
to spare myself in the pursuit of my interests and
responsibilities.

Therefore, I plan to resign from my current position as
Project Manager at VLEAD and enter into a consulting
contract without a discontinuity of my service at VLEAD.

During my time at VLEAD, I led the team in fulfilling all
the deliverables promised in phase 2 of the project.  I
worked along with Prof. Venkatesh in writing the proposal
for phase 3.

During phase 3, the current architecture that supports a
staged, incremental and federated experiment building is put
in place.  There is also more work as part of phase3 to
build experiments and the infrastructure that supports
experiment building.  I am committed to ensure these tasks
are accomplished.

I plan to work for 20 hours with an hourly rate that can be
fixed in the Rs 1200 - 1500/- range.  I assure you my
technical and professional diligence during my period of
engagement with VLEAD.  I am hoping for a six month contract
that is open for re-negotiation at the end of this period
contingent on my performance and work requirement.  I hope
the terms of the contract are fair to both the parties.

Please plan this transition at the earliest.
